#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Main program to solve
the EPFC Contest 15 2019 -- Le lâcher de boules de pétanque.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import sys

import brute


########
# Main #
########
def main():
    nb_ball = None
    nb_level = None
    optimization = len(sys.argv) > 3

    if len(sys.argv) > 1:
        nb_ball = int(sys.argv[1])
    if len(sys.argv) > 2:
        nb_level = int(sys.argv[2])

    if nb_ball is None:
        nb_ball = int(input())
    if nb_level is None:
        nb_level = int(input())

    nb_launch = (brute.solve_brute_optimized(nb_ball, nb_level) if optimization
                 else brute.solve_brute(nb_ball, nb_level))
    print(nb_launch)

if __name__ == '__main__':
    main()
