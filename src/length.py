# -*- coding: utf-8 -*-

"""
Implementation that use the S function to solve the problem.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import formulas


def solve_length(nb_ball: int, nb_level: int) -> int:
    """
    Solve the problem by using sum of lengths function
    and return the number of launches.
    """
    assert nb_ball >= 1, (nb_ball, nb_level)
    assert nb_level >= 0, (nb_ball, nb_level)

    nb_launch = 0

    while formulas.sum_length(nb_ball, nb_launch) < nb_level:
        nb_launch += 1

    return nb_launch
