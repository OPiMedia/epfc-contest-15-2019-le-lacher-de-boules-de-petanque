#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
EPFC Contest 15 2019 -- Le lâcher de boules de pétanque
Solution given Mars 28, 2019,
with some little cosmetic adaptations.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import math
import sys

sys.setrecursionlimit(10000)


__SOLVE_MEMO = dict()


def solve(n: int, e: int) -> int:
    assert n > 0, n
    assert e > 0, e

    n = min(n, math.floor(math.log(e, 2)) + 1)  # upper bound

    if (e == 1) or (n == 1):
        return e
    else:
        r = __SOLVE_MEMO.get((n, e), None)

        if r is None:
            if n == 2:  # special case based on 1 + 2 + 3 + ... + n
                s = 0
                r = 1
                while s < e:
                    s += r
                    r += 1

                return r - 1
            else:       # by recursion, with some simplifcations
                r = solve(n, e - 1)

                for k in range(n, (e + 1) // 2 + 1):
                    r = min(r, max(solve(n - 1, k - 1), solve(n, e - k)))

                r += 1
                __SOLVE_MEMO[(n, e)] = r

                return r
        else:
            return r


########
# Main #
########
def main():
    """
    Lit N et E sur l'entrée standard
    et imprime le résultat
    """
    # assert solve(1, 5) == 5
    # assert solve(2, 5) == 3, solve(2, 5)
    # assert solve(2, 100) == 14, solve(2, 100)
    # assert solve(5, 1000) == 11, solve(5, 1000)

    # assert solve(1, 5) == 5
    # assert solve(2, 5) == 3
    # assert solve(3, 5) == 3
    # assert solve(4, 5) == 3
    # assert solve(5, 5) == 3
    # assert solve(6, 5) == 3

    # assert solve(1, 1) == 1
    # assert solve(10, 1) == 1
    # assert solve(10, 9) == solve(9, 9)

    n = int(input())
    e = int(input())

    print(solve(n, e))

if __name__ == '__main__':
    main()
