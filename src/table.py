#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Print a table of results
in TSV or LaTeX table format.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import sys

import brute


########
# Main #
########
def main():
    diagonale = False
    header = False
    latex = False
    max_nb_ball = 20
    max_nb_level = 20

    sys.argv = sys.argv[1:]

    if (len(sys.argv) > 0) and (sys.argv[0] == '--diagonale'):
        diagonale = True
        sys.argv = sys.argv[1:]

    if (len(sys.argv) > 0) and (sys.argv[0] == '--header'):
        header = True
        sys.argv = sys.argv[1:]

    if (len(sys.argv) > 0) and (sys.argv[0] == '--latex'):
        latex = True
        sys.argv = sys.argv[1:]

    if len(sys.argv) > 0:
        max_nb_ball = int(sys.argv[0])
    if len(sys.argv) > 1:
        max_nb_level = int(sys.argv[1])

    sep = (' & ' if latex
           else '\t')
    end = ('\\\\\n' if latex
           else '\n')

    if header:
        print(sep + sep.join(str(n) for n in range(max_nb_level + 1)), end=end)
        if latex:
            print(r'\hline')

    for nb_ball in range(1, max_nb_ball + 1):
        line = ([str(nb_ball)] if latex
                else [])
        for nb_level in range(max_nb_level + 1):
            if not diagonale or (nb_ball <= nb_level):
                line.append(str(brute.solve_brute_optimized(nb_ball, nb_level)))
            else:
                line.append('')
        if latex:
            print(r'\hline')
        print(sep.join(line), end=end)

if __name__ == '__main__':
    main()
