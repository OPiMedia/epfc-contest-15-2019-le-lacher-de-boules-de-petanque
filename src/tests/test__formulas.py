#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
test__formulas.py

Unit tests of formulas.py.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import test_common

import brute
import formulas


def test__binomial():
    for i in range(101):
        assert formulas.binomial(i, 0) == 1
        assert formulas.binomial(i, i) == 1
        assert formulas.binomial(i, -1) == 0
        assert formulas.binomial(i, i + 1) == 0

    assert formulas.binomial(4, 2) == 6

    for n in range(101):
        for k in range(101):
            if (n != 0) or (k != 0):
                assert formulas.binomial(n, k) == (formulas.binomial(n - 1, k) +
                                                   formulas.binomial(n - 1, k - 1))


def test__length():
    for nb_ball in range(1, 101):
        for nb_launch in range(101):
            assert formulas.length(nb_ball, nb_launch) == formulas.length_recursive(nb_ball, nb_launch)


def test__length_recursive():
    for nb_ball in range(1, 101):
        lengthes = dict()
        for nb_level in range(101):
            nb_launch = brute.solve_brute(nb_ball, nb_level)
            lengthes[nb_launch] = lengthes.get(nb_launch, 0) + 1

        del lengthes[nb_launch]  # remove last (probably partial) result

        for nb_launch, nb in lengthes.items():
            assert formulas.length_recursive(nb_ball, nb_launch) == nb


def test__sum_length():
    for nb_ball in range(1, 101):
        for nb_launch in range(101):
            assert formulas.sum_length(nb_ball, nb_launch) == formulas.sum_length_recursive(nb_ball, nb_launch)


def test__sum_length_recursive():
    for nb_ball in range(1, 101):
        for nb_launch in range(101):
            sum_length = 0
            for i in range(1, nb_launch + 1):
                sum_length += formulas.length_recursive(nb_ball, i)

            assert formulas.sum_length_recursive(nb_ball, nb_launch) == sum_length


def test__m_2():
    assert formulas.m_2(0) == 0

    assert formulas.m_2(1) == 1

    assert formulas.m_2(2) == 2
    assert formulas.m_2(3) == 2

    assert formulas.m_2(4) == 3
    assert formulas.m_2(5) == 3
    assert formulas.m_2(6) == 3

    assert formulas.m_2(7) == 4
    assert formulas.m_2(8) == 4
    assert formulas.m_2(9) == 4
    assert formulas.m_2(10) == 4

    assert formulas.m_2(11) == 5

    for n in range(10001):
        assert formulas.m_2(formulas.triangular(n)) == n
        assert formulas.m_2(formulas.triangular(n) + 1) == n + 1

    for n in range(2, 10001):
        assert formulas.m_2(formulas.triangular(n) - 1) == n

    i = 1
    for length in range(1, 2001):
        for _ in range(length):
            assert formulas.m_2(i) == length
            i += 1


def test__triangular():
    assert formulas.triangular(0) == 0
    assert formulas.triangular(1) == 1
    assert formulas.triangular(2) == 3
    assert formulas.triangular(3) == 6
    assert formulas.triangular(4) == 10
    assert formulas.triangular(5) == 15

    assert formulas.triangular(100) == 5050
    assert formulas.triangular(101) == 5151

    for n in range(100):
        assert formulas.triangular(n) == sum(range(n + 1))


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing).
    test_common.run_tests(locals())
