#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
test__brute.py

Unit tests of brute.py.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import test_common

import brute
import formulas


def test__solve_brute():
    assert brute.solve_brute(1, 0) == 0
    assert brute.solve_brute(1, 1) == 1
    assert brute.solve_brute(1, 2) == 2
    assert brute.solve_brute(1, 3) == 3

    assert brute.solve_brute(2, 0) == 0
    assert brute.solve_brute(2, 1) == 1
    assert brute.solve_brute(2, 2) == 2
    assert brute.solve_brute(2, 3) == 2

    assert brute.solve_brute(3, 0) == 0
    assert brute.solve_brute(3, 1) == 1
    assert brute.solve_brute(3, 2) == 2
    assert brute.solve_brute(3, 3) == 2

    assert brute.solve_brute(1, 5) == 5
    assert brute.solve_brute(2, 5) == 3
    assert brute.solve_brute(2, 100) == 14
    assert brute.solve_brute(5, 1000) == 11

    for params, solution in test_common.EXAMPLES.items():
        assert brute.solve_brute(*params) == solution

    for nb_ball in range(1, 101):
        for nb_level in range(101):
            assert 0 <= brute.solve_brute(nb_ball, nb_level) <= nb_level
            if (nb_level > 0) and (nb_ball > nb_level):
                assert brute.solve_brute(nb_ball, nb_level) == brute.solve_brute(nb_level, nb_level)

    for nb_ball in range(1, 1001):
        assert brute.solve_brute(nb_ball, 0) == 0
        assert brute.solve_brute(nb_ball, 1) == 1

    for nb_level in range(1, 1001):
        assert brute.solve_brute(1, nb_level) == nb_level
        assert brute.solve_brute(2, nb_level) == formulas.m_2(nb_level)

    for i in range(51):
        assert brute.solve_brute(2, formulas.triangular(i)) == i
        assert brute.solve_brute(2, formulas.triangular(i) + 1) == i + 1

    for i in range(2, 51):
        assert brute.solve_brute(2, formulas.triangular(i) - 1) == i


def test__solve_brute_optimized():
    for params, solution in test_common.EXAMPLES.items():
        assert brute.solve_brute_optimized(*params) == solution

    for nb_ball in range(1, 101):
        for nb_level in range(101):
            assert brute.solve_brute_optimized(nb_ball, nb_level) == brute.solve_brute(nb_ball, nb_level)


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing).
    test_common.run_tests(locals())
