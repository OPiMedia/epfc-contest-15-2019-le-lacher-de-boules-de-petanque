#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
test_common.py

Common code for tests.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 24, 2019
"""

import sys

from typing import Callable, Dict, Iterable, Tuple


EXAMPLES = {(1, 5): 5,
            (2, 5): 3,
            (2, 100): 14,
            (5, 1000): 11}


def not_covered():
    """
    Print message to mark not covered function.
    """
    print('NOT covered', end=' ')


def run_tests(names: Dict[str, Callable]):
    """
    Run all `test__...()` functions (useful if pytest is missing).

    :param names:
    """
    names = dict(names)
    print('Collected {} items'
          .format(len([None for name in names if name.startswith('test__')])))
    for name in sorted(names):
        if name.startswith('test__'):
            print('{}::{} '.format(sys.argv[0], name), end='')
            sys.stdout.flush()
            names[name]()
            sys.stdout.flush()
            sys.stderr.flush()
            print()
    print('===== done =====')
