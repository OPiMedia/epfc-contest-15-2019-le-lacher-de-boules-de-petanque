#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
test__theory.py

Checking of some theoretical results.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import math

import test_common

import brute
import formulas


def test__conjectures():
    for nb_ball in range(1, 101):
        for nb_level in range(101):
            assert brute.solve_brute(nb_ball, nb_level + 1) - brute.solve_brute(nb_ball, nb_level) in (0, 1)

    for nb_launch in range(101):
        assert formulas.binomial(nb_launch + 1, 2) + formulas.binomial(nb_launch + 1, 4) == \
            nb_launch * (nb_launch + 1) * ((nb_launch - 2) * (nb_launch - 1) + 12) // 24

    for nb_level in range(101):
        assert brute.solve_brute(2, nb_level) == math.floor(math.sqrt(2 * nb_level) + 0.5)

    for nb_ball in range(1, 101):
        for k in range(9):
            nb_level = 2**k
            assert brute.solve_brute(nb_ball, nb_level) == \
                brute.solve_brute(min(nb_ball, k + 1), nb_level)

        for nb_level in range(1, 101):
            assert brute.solve_brute(nb_ball, nb_level) == \
                brute.solve_brute(min(nb_ball, math.floor(math.log(nb_level, 2)) + 1),
                                  nb_level)


def test__first_cases_nb_level():
    assert brute.solve_brute(1, 3) == 3
    assert brute.solve_brute(1, 4) == 4
    assert brute.solve_brute(1, 5) == 5
    assert brute.solve_brute(1, 6) == 6

    assert brute.solve_brute(1, 7) == 7
    assert brute.solve_brute(2, 7) == 4

    assert brute.solve_brute(1, 8) == 8
    assert brute.solve_brute(1, 9) == 9
    assert brute.solve_brute(1, 10) == 10

    assert brute.solve_brute(1, 11) == 11
    assert brute.solve_brute(2, 11) == 5

    for nb_ball in range(1, 101):
        assert brute.solve_brute(nb_ball, 0) == 0
        assert brute.solve_brute(nb_ball, 1) == 1
        assert brute.solve_brute(nb_ball, 2) == 2

        if nb_ball >= 2:
            assert brute.solve_brute(nb_ball, 3) == 2
            assert brute.solve_brute(nb_ball, 4) == 3
            assert brute.solve_brute(nb_ball, 5) == 3
            assert brute.solve_brute(nb_ball, 6) == 3

        if nb_ball >= 3:
            assert brute.solve_brute(nb_ball, 7) == 3

        if nb_ball >= 2:
            assert brute.solve_brute(nb_ball, 8) == 4
            assert brute.solve_brute(nb_ball, 9) == 4
            assert brute.solve_brute(nb_ball, 10) == 4
        if nb_ball >= 3:
            assert brute.solve_brute(nb_ball, 11) == 4


def test__inequalities():
    for nb_ball in range(1, 101):
        for nb_level in range(101):
            assert brute.solve_brute(nb_ball + 1, nb_level + 1) <= 1 + brute.solve_brute(nb_ball, nb_level)

            if nb_ball >= 4:  # TODO: check why it is true on these values
                for i in range(1, (nb_level + 2) // 2):
                    assert brute.solve_brute(nb_ball - 1, i - 1) <= 1 + brute.solve_brute(nb_ball, nb_level - i)

            if nb_ball >= 2:
                for i in range((nb_level + 2) // 2, nb_level + 1):
                    assert brute.solve_brute(nb_ball - 1, i - 1) >= brute.solve_brute(nb_ball, nb_level - i)


def test__inequality_fixed_balls():
    for nb_ball in range(1, 101):
        for nb_level in range(101):
            assert brute.solve_brute(nb_ball, nb_level + 1) >= brute.solve_brute(nb_ball, nb_level)

            for nb_level_b in range(101):
                if nb_level_b > nb_level:
                    assert brute.solve_brute(nb_ball, nb_level_b) >= brute.solve_brute(nb_ball, nb_level)
                elif nb_level_b < nb_level:
                    assert brute.solve_brute(nb_ball, nb_level_b) <= brute.solve_brute(nb_ball, nb_level)

                if brute.solve_brute(nb_ball, nb_level_b) > brute.solve_brute(nb_ball, nb_level):
                    assert nb_level_b > nb_level


def test__inequalities_fixed_levels():
    for nb_ball in range(1, 101):
        for nb_level in range(101):
            assert brute.solve_brute(nb_ball + 1, nb_level) <= brute.solve_brute(nb_ball, nb_level)
            if nb_ball >= nb_level >= 1:
                assert brute.solve_brute(nb_level, nb_level) == brute.solve_brute(nb_ball, nb_level)
            if nb_level >= 1:
                assert brute.solve_brute(nb_level, nb_level) <= brute.solve_brute(nb_ball, nb_level)

            for nb_ball_b in range(1, 101):
                if nb_ball_b > nb_ball:
                    assert brute.solve_brute(nb_ball_b, nb_level) <= brute.solve_brute(nb_ball, nb_level)
                elif nb_ball_b < nb_ball:
                    assert brute.solve_brute(nb_ball_b, nb_level) >= brute.solve_brute(nb_ball, nb_level)

                if brute.solve_brute(nb_ball_b, nb_level) < brute.solve_brute(nb_ball, nb_level):
                    assert nb_ball_b > nb_ball


def test__lengthes():
    for nb_ball in range(1, 5):
        launches = []
        correct = []
        for nb_level in range(101):
            nb_launch = brute.solve_brute(nb_ball, nb_level)
            launches.append(nb_launch)
            correct.extend([nb_level] * formulas.length(nb_ball, nb_level))

        assert launches == correct[:len(launches)]


def test__upper_bound():
    for nb_ball in range(1, 101):
        for nb_level in range(101):
            nb_launch = brute.solve_brute(nb_ball, nb_level)
            assert 0 <= nb_launch <= nb_level
            if nb_launch in (0, 1):
                assert nb_level == nb_launch


def test__upper_bound_lg():
    for nb_ball in range(1, 101):
        for nb_level in range(1, 101):
            assert brute.solve_brute(nb_ball, nb_level) == \
                brute.solve_brute(min(nb_ball, math.floor(math.log(nb_level, 2)) + 1), nb_level)

        for k in range(8):
            nb_level = 2**k
            assert brute.solve_brute(nb_ball, nb_level) == \
                brute.solve_brute(min(nb_ball, k + 1), nb_level)


def test__upper_bound_equivalence():
    for nb_ball in range(1, 101):
        for nb_level in range(101):
            n = brute.solve_brute(nb_ball, nb_level)
            if n >= 1:
                assert brute.solve_brute(min(nb_ball, n), nb_level) == n, (nb_ball, nb_level, n)


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing).
    test_common.run_tests(locals())
