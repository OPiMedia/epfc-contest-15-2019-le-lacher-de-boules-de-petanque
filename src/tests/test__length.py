#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
test__length.py

Unit tests of length.py.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import test_common

import brute
import length


def test__solve_length():
    for params, solution in test_common.EXAMPLES.items():
        assert length.solve_length(*params) == solution

    for nb_ball in range(1, 101):
        for nb_level in range(101):
            assert length.solve_length(nb_ball, nb_level) == brute.solve_brute(nb_ball, nb_level)


########
# Main #
########
if __name__ == '__main__':
    # Run all test__...() functions (useful if pytest is missing).
    test_common.run_tests(locals())
