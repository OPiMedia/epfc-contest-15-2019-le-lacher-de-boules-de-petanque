# -*- coding: utf-8 -*-

"""
Some general functions
and implementation of l and S functions.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import math


__LENGTH_MEMO = dict()
__LENGTH_RECURSIVE_MEMO = dict()


def binomial(n: int, k: int) -> int:
    """
    Return the binomial coefficient n! / (k! (n-k)!).
    """
    if 0 <= k <= n:
        numerator = 1
        denominator = 1
        for i in range(1, min(k, n - k) + 1):
            numerator *= n
            n -= 1
            denominator *= i

        return numerator // denominator
    else:
        return 0


def length(nb_ball: int, nb_launch: int) -> int:
    """
    Same that length_recursive()
    but with direct computations for nb_ball <= 3.
    """
    assert nb_ball >= 1, (nb_ball, nb_launch)
    assert nb_launch >= 0, (nb_ball, nb_launch)

    if nb_launch > 1:
        if nb_ball == 1:
            return 1
        elif nb_ball == 2:
            return nb_launch
        elif nb_ball == 3:
            return 1 + (nb_launch - 1) * nb_launch // 2
        else:
            result = __LENGTH_MEMO.get((nb_ball, nb_launch), None)

            if result is None:
                result = length(nb_ball - 1, nb_launch - 1) + length(nb_ball, nb_launch - 1)
                __LENGTH_MEMO[(nb_ball, nb_launch)] = result

            return result
    else:
        return 1


def length_recursive(nb_ball: int, nb_launch: int) -> int:
    """
    Return the value of the length function,
    i.e. the number of appearances of the number of launches.
    """
    assert nb_ball >= 1, (nb_ball, nb_launch)
    assert nb_launch >= 0, (nb_ball, nb_launch)

    if (nb_ball >= 2) and (nb_launch >= 2):
        result = __LENGTH_RECURSIVE_MEMO.get((nb_ball, nb_launch), None)

        if result is None:
            result = (length_recursive(nb_ball - 1, nb_launch - 1) +
                      length_recursive(nb_ball, nb_launch - 1))
            __LENGTH_RECURSIVE_MEMO[(nb_ball, nb_launch)] = result

        return result
    else:
        return 1


def m_2(n: int) -> int:
    """
    The Kruskal-Macaulay function M_2(n):
    https://oeis.org/A123578
    0, 1, 2,2, 3,3,3, 4,4,4,4, 5,5,5,5,5, 6,6,6,6,6,6, 7,7,7,7,7,7,7, 8...
    """
    return math.floor(math.sqrt(2 * n) + 0.5)


def sum_length(nb_ball: int, nb_launch: int) -> int:
    """
    Same that sum_length_recursive()
    but with direct computations for nb_ball <= 3
    and use of sum_length() instead sum_length_recursive().
    """
    assert nb_ball >= 1, (nb_ball, nb_launch)
    assert nb_launch >= 0, (nb_ball, nb_launch)

    if nb_ball == 1:
        return nb_launch
    elif nb_ball == 2:
        return nb_launch * (nb_launch + 1) // 2
    elif nb_ball == 3:
        return nb_launch * (nb_launch**2 + 5) // 6
    elif nb_ball == 4:
        return binomial(nb_launch + 1, 2) + binomial(nb_launch + 1, 4)
    else:
        return sum(length(nb_ball, i) for i in range(1, nb_launch + 1))


def sum_length_recursive(nb_ball: int, nb_launch: int) -> int:
    """
    Sum of the first nb_launch values length_recursive(nb_ball, i).
    """
    assert nb_ball >= 1, (nb_ball, nb_launch)
    assert nb_launch >= 0, (nb_ball, nb_launch)

    return sum(length_recursive(nb_ball, i) for i in range(1, nb_launch + 1))


def triangular(n: int) -> int:
    """
    Return the sum of integers 0, 1, 2 ... n.

    Triangular number T(n):
    https://oeis.org/A000217
    0,1,3,6,10,15,21,28,36,45,55,66,78,91,105,120,136,153,171,190,210,231,253...
    """
    assert n >= 0, n

    return n * (n + 1) // 2


def upper_bound(nb_ball: int, nb_level: int) -> int:
    """
    Return an upper bound of the solution depending of nb_ball and nb_level.

    :param nb_ball: int >= 1
    :param nb_level: int >= 1
    """
    assert nb_ball >= 1, (nb_ball, nb_level)
    assert nb_level >= 1, (nb_ball, nb_level)

    return min(nb_ball, math.floor(math.log(nb_level, 2)) + 1)
