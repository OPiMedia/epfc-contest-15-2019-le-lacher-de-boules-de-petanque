# -*- coding: utf-8 -*-

"""
Recursive implementations
directly taken from the recursive function L of the problem,
or with some optimizations.

Both with memoization.

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: April 26, 2019
"""

import sys

import formulas

sys.setrecursionlimit(10000)

__SOLVE_BRUTE_MEMO = dict()
__SOLVE_BRUTE_OPTIMIZED_MEMO = dict()


def solve_brute(nb_ball: int, nb_level: int) -> int:
    """
    Solve the problem by exhaustive tries
    and return the number of launches.
    """
    assert nb_ball >= 1, (nb_ball, nb_level)
    assert nb_level >= 0, (nb_ball, nb_level)

    if (nb_ball > 1) and (nb_level > 0):  # recursive case
        nb_launch = __SOLVE_BRUTE_MEMO.get((nb_ball, nb_level), None)

        if nb_launch is None:
            nb_launch = nb_level

            for level in range(1, nb_level + 1):
                nb_launch = min(nb_launch,
                                1 + max(solve_brute(nb_ball - 1, level - 1),      # broken
                                        solve_brute(nb_ball, nb_level - level)))  # not broken

            __SOLVE_BRUTE_MEMO[(nb_ball, nb_level)] = nb_launch

        return nb_launch
    else:                                 # base cases
        return nb_level


def solve_brute_optimized(nb_ball: int, nb_level: int) -> int:
    """
    Optimized version of solve_brute().
    """
    assert nb_ball >= 1, (nb_ball, nb_level)
    assert nb_level >= 0, (nb_ball, nb_level)

    if nb_level > 0:
        if (1 << nb_ball) >= nb_level:
            nb_ball = formulas.upper_bound(nb_ball, nb_level)

        if nb_ball > 2:     # recursive case
            nb_launch = __SOLVE_BRUTE_OPTIMIZED_MEMO.get((nb_ball, nb_level), None)

            if nb_launch is None:
                middle = nb_level // 2
                nb_launch = 1 + solve_brute_optimized(nb_ball - 1, middle)

                for level in range(1, middle + 1):
                    nb_launch = min(nb_launch,
                                    1 + max(solve_brute_optimized(nb_ball - 1, level - 1),      # broken
                                            solve_brute_optimized(nb_ball, nb_level - level)))  # not broken

                __SOLVE_BRUTE_OPTIMIZED_MEMO[(nb_ball, nb_level)] = nb_launch

            return nb_launch
        elif nb_ball == 2:  # specific case
            return formulas.m_2(nb_level)

    # Base cases (nb_ball == 1 or nb_level == 0)
    return nb_level
