.. -*- restructuredtext -*-

======================================================
EPFC Contest 15 2019 — Le lâcher de boules de pétanque
======================================================

* `Enonce`_ and `Solutions`_ by Alain Silovy (EPFC_)
* My `Quick Report`_ about the EPFC 15th Contest — Le lâcher de boules de pétanque
* My `Python code`_

.. _`Enonce`: https://bitbucket.org/OPiMedia/epfc-contest-15-2019-le-lacher-de-boules-de-petanque/raw/master/EPFC/EPFC-15th-contest-2019-Le-lacher-de-boules-de-petanque--Enonce--Alain-Silovy.pdf
.. _EPFC: https://www.epfc.eu/
.. _`Python code`: https://bitbucket.org/OPiMedia/epfc-contest-15-2019-le-lacher-de-boules-de-petanque/src/master/src/
.. _`Quick Report`: https://bitbucket.org/OPiMedia/epfc-contest-15-2019-le-lacher-de-boules-de-petanque/raw/master/EPFC-Contest-15-2019-petanque--report--Olivier-Pirson.pdf
.. _`Solutions`: https://bitbucket.org/OPiMedia/epfc-contest-15-2019-le-lacher-de-boules-de-petanque/raw/master/EPFC/EPFC-15th-contest-2019-Le-lacher-de-boules-de-petanque--Solution--Alain-Silovy.pdf

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/workspace/repositories

* 📧 olivier.pirson.opi@gmail.com
* 🧑‍🤝‍🧑 Bluesky: https://bsky.app/profile/opimedia.bsky.social — Mastodon: https://mamot.fr/@OPiMedia — X/Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



License: GPLv3_ |GPLv3|
=======================
Copyright (C) 2019, 2024 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png



|petanque|

(extract from this picture_)

.. _picture: https://commons.wikimedia.org/wiki/File:Sascha_Grosser_-_Boules-obut-sascha-grosser-asom1-f1024-hdr-sw2.jpg

.. |petanque| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/16/2691695000-4-epfc-contest-15-2019-le-lacher-de-bou_avatar.png
